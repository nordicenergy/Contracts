
## Energy Smart Contracts

##### Nordic Energy exchange use case.

Nordic Energy Token: https://etherscan.io/token/0x4d0a4c762bd7f742096daaf5911dcf9c94b9ea95

Network Manager Contract: https://etherscan.io/address/0x55d18731286af2daa27d50a1d15f5d77512e6723#code

Energy Store: https://etherscan.io/address/0xf5dbfcfb221170dc81feda0254b4c660a6c3f7d2

Migrations: https://etherscan.io/address/0xcab1b8c159fa699b4e4977abfa6970ca72bf6b2a

Consumer Registry: https://etherscan.io/address/0xf9e31bbfbac1e46cd429a5308693c4ca3c8ec15b

Owned: https://etherscan.io/address/0x55d18731286af2daa27d50a1d15f5d77512e6723
